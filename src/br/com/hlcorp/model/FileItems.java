package br.com.hlcorp.model;

public class FileItems {
	private String cpnjEmpresa;
	private String cidadeEmpresa;
	private String dataVenda;
	private String dataEfetivacao;
	private int idPedido;
	private String cpfCnpjCliente;
	private String nomeCliente;
	private String enderecoCliente;
	private String numeroEnderecoCliente;
	private String complementoEnderecoCliente;
	private String bairroCliente;
	private String cidadeCliente;
	private String codigoIBGECidade;
	private String ufCidade;
	private String cep;
	private String emailCliente;
	private String telefoneCliente;
	private int codigoPacote;
	private String nomeCursoPacote;
	int quantidadeCursoPacote;
	private String valorCobradoPacote;
	private String transactionIdPgm;
	private int numeroParcelas;
	private String tipoTransacao;
	
	public FileItems() {}
	
	public FileItems(String cpnjEmpresa, String cidadeEmpresa, String dataVenda, String dataEfetivacao, int idPedido,
			String cpfCnpjCliente, String nomeCliente, String enderecoCliente, String numeroEnderecoCliente,
			String complementoEnderecoCliente, String bairroCliente, String cidadeCliente, String codigoIBGECidade,
			String ufCidade, String cep, String emailCliente, String telefoneCliente, int codigoPacote,
			String nomeCursoPacote, int quantidadeCursoPacote, String valorCobradoPacote,
			String transactionIdPgm, int numeroParcelas, String tipoTransacao) {
		this.cpnjEmpresa = cpnjEmpresa;
		this.cidadeEmpresa = cidadeEmpresa;
		this.dataVenda = dataVenda;
		this.dataEfetivacao = dataEfetivacao;
		this.idPedido = idPedido;
		this.cpfCnpjCliente = cpfCnpjCliente;
		this.nomeCliente = nomeCliente;
		this.enderecoCliente = enderecoCliente;
		this.numeroEnderecoCliente = numeroEnderecoCliente;
		this.complementoEnderecoCliente = complementoEnderecoCliente;
		this.bairroCliente = bairroCliente;
		this.cidadeCliente = cidadeCliente;
		this.codigoIBGECidade = codigoIBGECidade;
		this.ufCidade = ufCidade;
		this.cep = cep;
		this.emailCliente = emailCliente;
		this.telefoneCliente = telefoneCliente;
		this.codigoPacote = codigoPacote;
		this.nomeCursoPacote = nomeCursoPacote;
		this.quantidadeCursoPacote = quantidadeCursoPacote;
		this.valorCobradoPacote = valorCobradoPacote;
		this.transactionIdPgm = transactionIdPgm;
		this.numeroParcelas = numeroParcelas;
		this.tipoTransacao = tipoTransacao;
	}
	public String getCpnjEmpresa() {
		return cpnjEmpresa;
	}
	public void setCpnjEmpresa(String cpnjEmpresa) {
		this.cpnjEmpresa = cpnjEmpresa;
	}
	public String getCidadeEmpresa() {
		return cidadeEmpresa;
	}
	public void setCidadeEmpresa(String cidadeEmpresa) {
		this.cidadeEmpresa = cidadeEmpresa;
	}
	public String getDataVenda() {
		return dataVenda;
	}
	public void setDataVenda(String dataVenda) {
		this.dataVenda = dataVenda;
	}
	public String getDataEfetivacao() {
		return dataEfetivacao;
	}
	public void setDataEfetivacao(String dataEfetivacao) {
		this.dataEfetivacao = dataEfetivacao;
	}
	public int getIdPedido() {
		return idPedido;
	}
	public void setIdPedido(int idPedido) {
		this.idPedido = idPedido;
	}
	public String getCpfCnpjCliente() {
		return cpfCnpjCliente;
	}
	public void setCpfCnpjCliente(String cpfCnpjCliente) {
		this.cpfCnpjCliente = cpfCnpjCliente;
	}
	public String getNomeCliente() {
		return nomeCliente;
	}
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}
	public String getEnderecoCliente() {
		return enderecoCliente;
	}
	public void setEnderecoCliente(String enderecoCliente) {
		this.enderecoCliente = enderecoCliente;
	}
	public String getNumeroEnderecoCliente() {
		return numeroEnderecoCliente;
	}
	public void setNumeroEnderecoCliente(String numeroEnderecoCliente) {
		this.numeroEnderecoCliente = numeroEnderecoCliente;
	}
	public String getComplementoEnderecoCliente() {
		return complementoEnderecoCliente;
	}
	public void setComplementoEnderecoCliente(String complementoEnderecoCliente) {
		this.complementoEnderecoCliente = complementoEnderecoCliente;
	}
	public String getBairroCliente() {
		return bairroCliente;
	}
	public void setBairroCliente(String bairroCliente) {
		this.bairroCliente = bairroCliente;
	}
	public String getCidadeCliente() {
		return cidadeCliente;
	}
	public void setCidadeCliente(String cidadeCliente) {
		this.cidadeCliente = cidadeCliente;
	}
	public String getCodigoIBGECidade() {
		return codigoIBGECidade;
	}
	public void setCodigoIBGECidade(String codigoIBGECidade) {
		this.codigoIBGECidade = codigoIBGECidade;
	}
	public String getUfCidade() {
		return ufCidade;
	}
	public void setUfCidade(String ufCidade) {
		this.ufCidade = ufCidade;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getEmailCliente() {
		return emailCliente;
	}
	public void setEmailCliente(String emailCliente) {
		this.emailCliente = emailCliente;
	}
	public String getTelefoneCliente() {
		return telefoneCliente;
	}
	public void setTelefoneCliente(String telefoneCliente) {
		this.telefoneCliente = telefoneCliente;
	}
	public int getCodigoPacote() {
		return codigoPacote;
	}
	public void setCodigoPacote(int codigoPacote) {
		this.codigoPacote = codigoPacote;
	}
	public String getNomeCursoPacote() {
		return nomeCursoPacote;
	}
	public void setNomeCursoPacote(String nomeCursoPacote) {
		this.nomeCursoPacote = nomeCursoPacote;
	}
	public int getQuantidadeCursoPacote() {
		return quantidadeCursoPacote;
	}
	public void setQuantidadeCursoPacote(int quantidadeCursoPacote) {
		this.quantidadeCursoPacote = quantidadeCursoPacote;
	}
	public String getValorCobradoPacote() {
		return valorCobradoPacote;
	}
	public void setValorCobradoPacote(String valorCobradoPacote) {
		this.valorCobradoPacote = valorCobradoPacote;
	}
	public String getTransactionIdPgm() {
		return transactionIdPgm;
	}
	public void setTransactionIdPgm(String transactionIdPgm) {
		this.transactionIdPgm = transactionIdPgm;
	}
	public int getNumeroParcelas() {
		return numeroParcelas;
	}
	public void setNumeroParcelas(int numeroParcelas) {
		this.numeroParcelas = numeroParcelas;
	}
	public String getTipoTransacao() {
		return tipoTransacao;
	}
	public void setTipoTransacao(String tipoTransacao) {
		this.tipoTransacao = tipoTransacao;
	}
}
