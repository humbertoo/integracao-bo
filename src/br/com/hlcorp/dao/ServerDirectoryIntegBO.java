package br.com.hlcorp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

public class ServerDirectoryIntegBO {
	private static final Logger LOGGER = Logger.getLogger(ServerDirectoryIntegBO.class);
	
	public static String getServerDirectoryIntegBO(Connection connection) {
		String directory = "";
		
		try {
			String sql = "SELECT TEXTO FROM TSIPAR WHERE CHAVE ='TPEDIRINTEGBO'";
			PreparedStatement ps = connection.prepareStatement(sql);		
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				directory = rs.getString(1);
			}
			
			ps.close();
		} catch (SQLException e) {
			LOGGER.error(e.toString());
			System.exit(0);
		}
		
		return directory;
	}
}
