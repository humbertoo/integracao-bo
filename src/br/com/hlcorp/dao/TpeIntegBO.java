package br.com.hlcorp.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import br.com.hlcorp.model.FileItems;

public class TpeIntegBO {
	private static final Logger LOGGER = Logger.getLogger(TpeIntegBO.class);
	
	private static PreparedStatement ps;
	
	public static boolean insert(Connection connection, String fileName, FileItems fileItems, int i) {	
		boolean inserted = false;
		
		try {
			connection.setAutoCommit(false);
			
			/*String sqlInsert = "INSERT INTO AD_TPEINTEGBO ("+ 
			        "  NUMERO_UNICO,\r\n" + 
					"  NOME_ARQUIVO,\r\n" + 
					"  LINHA,\r\n" +
					"  DATA_INCLUSAO,\r\n" + 
					"  PROCESSADO,\r\n" + 
					"  NUNOTA_PROD,\r\n" + 
					"  NUNOTA_SERV,\r\n" + 
					"  CNPJ_EMP,\r\n" + 
					"  NOME_CIDADE_EMP,\r\n" + 
					"  DATA_VENDA_PED,\r\n" + 
					"  DATA_EFETIVACAO_PED,\r\n" + 
					"  ID_PEDIDO,\r\n" + 
					"  CPF_CNPJ_CLI,\r\n" +
					"  NOME_CLI,\r\n" + 
					"  NOME_ENDERECO_CLI,\r\n" + 
					"  NRO_ENDERECO_CLI,\r\n" + 
					"  COMPL_ENDERECO_CLI,\r\n" + 
					"  NOME_BAIRRO_CLI,\r\n" + 
					"  NOME_CIDADE_CLI,\r\n" + 
					"  CODIBGE_CIDADE_CLI,\r\n" + 
					"  UF_CLI,\r\n" + 
					"  CEP_CLI,\r\n" + 
					"  EMAIL_CLI,\r\n" + 
					"  TEL_CLI,\r\n" + 
					"  COD_PACOTE,\r\n" + 
					"  NOME_CURSO_PACOTE,\r\n" + 
					"  QTDE_CURSO_PACOTE,\r\n" + 
					"  VLR_COBRADO_PACOTE,\r\n" +
					"  TIPO_TRANSACAO,\r\n" + 
					"  TRANSACTION_ID_PGM,\r\n" + 
					"  NRO_PARCELAS)\r\n" + 
					" VALUES (?,?,?,SYSDATE,'N',0,0,?,?,TO_DATE(?, 'DD/MM/YYYY'),TO_DATE(?, 'DD/MM/YYYY'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";*/
			
			String sqlInsert = "INSERT INTO AD_TPEINTEGBO ("+ 
			        "  NUMERO_UNICO,\r\n" + 
					"  NOME_ARQUIVO,\r\n" + 
					"  LINHA,\r\n" +
					"  DATA_INCLUSAO,\r\n" + 
					"  PROCESSADO,\r\n" + 
					"  NUNOTA_PROD,\r\n" + 
					"  NUNOTA_SERV,\r\n" + 
					"  CNPJ_EMP,\r\n" + 
					"  NOME_CIDADE_EMP,\r\n" + 
					"  DATA_VENDA_PED,\r\n" + 
					"  DATA_EFETIVACAO_PED,\r\n" + 
					"  ID_PEDIDO,\r\n" + 
					"  CPF_CNPJ_CLI,\r\n" +
					"  NOME_CLI,\r\n" + 
					"  NOME_ENDERECO_CLI,\r\n" + 
					"  NRO_ENDERECO_CLI,\r\n" + 
					"  COMPL_ENDERECO_CLI,\r\n" + 
					"  NOME_BAIRRO_CLI,\r\n" + 
					"  NOME_CIDADE_CLI,\r\n" + 
					"  CODIBGE_CIDADE_CLI,\r\n" + 
					"  UF_CLI,\r\n" + 
					"  CEP_CLI,\r\n" + 
					"  EMAIL_CLI,\r\n" + 
					"  TEL_CLI,\r\n" + 
					"  COD_PACOTE,\r\n" + 
					"  NOME_CURSO_PACOTE,\r\n" + 
					"  QTDE_CURSO_PACOTE,\r\n" + 
					"  VLR_COBRADO_PACOTE,\r\n" +
					"  TIPO_TRANSACAO,\r\n" + 
					"  TRANSACTION_ID_PGM,\r\n" + 
					"  NRO_PARCELAS)\r\n" + 
					" VALUES (?,?,?,SYSDATE,'N',0,0,?,?,?,?,?,?,?,?,?,?,?,?,Nvl(?, 0),?,?,?,?,?,?,?,?,?,?,?)";
			
			TpeIntegBO.ps = connection.prepareStatement(sqlInsert);
			
			TpeIntegBO.ps.setInt(1, getPrimaryKey(connection));
			TpeIntegBO.ps.setString(2, fileName);
			TpeIntegBO.ps.setInt(3, i);
			TpeIntegBO.ps.setString(4, fileItems.getCpnjEmpresa());
			TpeIntegBO.ps.setString(5, fileItems.getCidadeEmpresa());
			TpeIntegBO.ps.setString(6, fileItems.getDataVenda());
			TpeIntegBO.ps.setString(7, fileItems.getDataEfetivacao());
			TpeIntegBO.ps.setInt(8, fileItems.getIdPedido());
			TpeIntegBO.ps.setString(9, fileItems.getCpfCnpjCliente());
			TpeIntegBO.ps.setString(10, fileItems.getNomeCliente());
			TpeIntegBO.ps.setString(11, fileItems.getEnderecoCliente());
			TpeIntegBO.ps.setString(12, fileItems.getNumeroEnderecoCliente());
			TpeIntegBO.ps.setString(13, fileItems.getComplementoEnderecoCliente());
			TpeIntegBO.ps.setString(14, fileItems.getBairroCliente());
			TpeIntegBO.ps.setString(15, fileItems.getCidadeCliente());
			TpeIntegBO.ps.setString(16, fileItems.getCodigoIBGECidade());
			TpeIntegBO.ps.setString(17, fileItems.getUfCidade());
			TpeIntegBO.ps.setString(18, fileItems.getCep());
			TpeIntegBO.ps.setString(19, fileItems.getEmailCliente());
			TpeIntegBO.ps.setString(20, fileItems.getTelefoneCliente());
			TpeIntegBO.ps.setInt(21, fileItems.getCodigoPacote());
			TpeIntegBO.ps.setString(22, fileItems.getNomeCursoPacote());
			TpeIntegBO.ps.setInt(23, fileItems.getQuantidadeCursoPacote());
			TpeIntegBO.ps.setBigDecimal(24, new BigDecimal(fileItems.getValorCobradoPacote()));
			TpeIntegBO.ps.setString(25, fileItems.getTipoTransacao());
			TpeIntegBO.ps.setString(26, fileItems.getTransactionIdPgm());
			TpeIntegBO.ps.setInt(27, fileItems.getNumeroParcelas());
				
			TpeIntegBO.ps.execute();
			
			connection.commit();
			
			inserted = true;
		} catch (SQLException e) {
			LOGGER.error(e.toString());
			
			if(connection != null) {
				try {
					connection.rollback();
				} catch (SQLException x) {
					LOGGER.error(x.toString());
				}
			}
		} finally {
			try {
				if(TpeIntegBO.ps != null) {
					TpeIntegBO.ps.close();
				}
				
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				LOGGER.error(e.toString());
			}
		}
		
		return inserted;
	}
	
	public static int getPrimaryKey(Connection connection) {
		int primaryKey = 0;
		
		try {
			String sql = "SELECT NVL(MAX(NUMERO_UNICO), 0) + 1 NUMERO_UNICO FROM AD_TPEINTEGBO";
			PreparedStatement ps = connection.prepareStatement(sql);		
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				primaryKey = rs.getInt(1);
			}
			
			ps.close();
		} catch (SQLException e) {
			LOGGER.error(e.toString());
			System.exit(0);
		}
		
		return primaryKey;
	}
}
