package br.com.hlcorp.file;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import br.com.hlcorp.model.FileItems;

public class ReadFile {
	public ArrayList<FileItems> readFileCSV(String filename) throws IOException {	
		ArrayList<FileItems> items = new ArrayList<>();
		
		Reader reader = Files.newBufferedReader(Paths.get(filename));
		
		CSVParser parser = new CSVParserBuilder()
				.withSeparator('|')
				.withIgnoreQuotations(true)
				.build();
		
		CSVReader csvReader = new CSVReaderBuilder(reader)
				.withSkipLines(1)
				.withCSVParser(parser)
				.build();
				
		List<String[]> rows = csvReader.readAll();
		
		for(String[] row : rows) {
			FileItems fileItem = new FileItems();
			
			fileItem.setCpnjEmpresa(row[0]);
			fileItem.setCidadeEmpresa(row[1].replace("\"", ""));
			fileItem.setDataVenda(row[2]);
			fileItem.setDataEfetivacao(row[3]);
			fileItem.setIdPedido(Integer.parseInt(row[4]));
			fileItem.setCpfCnpjCliente(row[5]);
			fileItem.setNomeCliente(row[6].replace("\"", ""));
			fileItem.setEnderecoCliente(row[7].replace("\"", ""));
			fileItem.setNumeroEnderecoCliente(row[8].replace("\"", ""));
			fileItem.setComplementoEnderecoCliente(row[9].replace("\"", ""));
			fileItem.setBairroCliente(row[10].replace("\"", ""));
			fileItem.setCidadeCliente(row[11].replace("\"", ""));
			fileItem.setCodigoIBGECidade(row[12].equals("")?"0":row[12]);
			fileItem.setUfCidade(row[13].replace("\"", ""));
			fileItem.setCep(row[14].replace("\"", ""));
			fileItem.setEmailCliente(row[15].replace("\"", ""));
			fileItem.setTelefoneCliente(row[16]);
			fileItem.setCodigoPacote(Integer.parseInt(row[17]));
			fileItem.setNomeCursoPacote(row[18].replace("\"", ""));
			//fileItem.setQuantidadeCursoPacote(Integer.parseInt(row[19]));
			fileItem.setQuantidadeCursoPacote(Integer.parseInt("1"));
			fileItem.setValorCobradoPacote(row[19]);
			fileItem.setTipoTransacao(row[20].replace("\"", ""));
			fileItem.setTransactionIdPgm(row[21]);
			fileItem.setNumeroParcelas(Integer.parseInt(row[22]));
			
			items.add(fileItem);
		}
		
		return items;
	}
}