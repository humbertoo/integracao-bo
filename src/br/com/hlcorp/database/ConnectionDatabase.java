package br.com.hlcorp.database;

import java.sql.*;

import org.apache.log4j.Logger;

import br.com.hlcorp.IntegracaoBO;

public class ConnectionDatabase {

	private static final Logger LOGGER = Logger.getLogger(IntegracaoBO.class);
	
	public static Connection getConnection(String url, String user, String password) {
		Connection connection = null;
		
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			
			//connection = DriverManager.getConnection("jdbc:oracle:thin:@NOTE-HUMBERTO:1521:ORCL", "SANKHYA", "tecsis");
			connection = DriverManager.getConnection("jdbc:oracle:thin:@" + url, user, password);
			
			LOGGER.info("Conectado ao Banco de Dados: " + url);
		} catch (ClassNotFoundException e) {
			LOGGER.error(e.toString());
			System.exit(0);
		} catch (SQLException e) {
			LOGGER.error(e.toString());
			System.exit(0);
		}
		
		return connection;
	}
	
}
