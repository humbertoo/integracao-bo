package br.com.hlcorp.util;

public class Constants {
	public static final String SUFIX_FILE = ".CSV";
	public static final String COLUMN_NAME_CNPJ_EMP = "CNPJ_EMP";
	public static final String COLUMN_NAME_NOME_CIDADE_EMP = "NOME_CIDADE_EMP";
	public static final String COLUMN_NAME_DATA_VENDA_PED = "DATA_VENDA_PED";
	public static final String COLUMN_NAME_DATA_EFETIVACAO_PED = "DATA_EFETIVACAO_PED";
	public static final String COLUMN_NAME_ID_PEDIDO = "ID_PEDIDO";
	public static final String COLUMN_NAME_CPF_CNPJ_CLI = "CPF_CNPJ_CLI";
	public static final String COLUMN_NAME_NOME_CLI = "NOME_CLI";
	public static final String COLUMN_NAME_NOME_ENDERECO_CLI = "NOME_ENDERECO_CLI";
	public static final String COLUMN_NAME_NRO_ENDERECO_CLI = "NRO_ENDERECO_CLI";
	public static final String COLUMN_NAME_COMPL_ENDERECO_CLI = "COMPL_ENDERECO_CLI";
	public static final String COLUMN_NAME_NOME_BAIRRO_CLI = "NOME_BAIRRO_CLI";
	public static final String COLUMN_NAME_NOME_CIDADE_CLI = "NOME_CIDADE_CLI";
	public static final String COLUMN_NAME_CODIBGE_CIDADE_CLI = "CODIBGE_CIDADE_CLI";
	public static final String COLUMN_NAME_UF_CLI = "UF_CLI";
	public static final String COLUMN_NAME_CEP_CLI = "CEP_CLI";
	public static final String COLUMN_NAME_EMAIL_CLI = "EMAIL_CLI";
	public static final String COLUMN_NAME_TEL_CLI = "TEL_CLI";
	public static final String COLUMN_NAME_COD_PACOTE = "COD_PACOTE";
	public static final String COLUMN_NAME_NOME_CURSO_PACOTE = "NOME_CURSO_PACOTE";
	public static final String COLUMN_NAME_QTDE_CURSO_PACOTE = "QTDE_CURSO_PACOTE";
	public static final String COLUMN_NAME_VLR_COBRADO_PACOTE = "VLR_COBRADO_PACOTE";
	public static final String COLUMN_NAME_TRANSACTION_ID_PGM = "TRANSACTION_ID_PGM";
	public static final String COLUMN_NAME_NRO_PARCELAS = "NRO_PARCELAS";
}