package br.com.hlcorp;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import org.apache.log4j.Logger;

import br.com.hlcorp.dao.ServerDirectoryIntegBO;
import br.com.hlcorp.dao.TpeIntegBO;
import br.com.hlcorp.database.ConnectionDatabase;
import br.com.hlcorp.file.ListFiles;
import br.com.hlcorp.file.ReadFile;
import br.com.hlcorp.model.FileItems;
import jxl.read.biff.BiffException;

public class IntegracaoBO {
	private static final Logger LOGGER = Logger.getLogger(IntegracaoBO.class);
	private static String directory = null;

	public static void main(String[] args) throws IOException, SQLException, BiffException {	
		String url, user, password;
		
		url = args[0];
		user = args[1];
		password = args[2];
		
		LOGGER.info("Iniciando a Integra��o BO");
		
		if (url.isEmpty() || url.equals("")) {
			LOGGER.error("A URL de Conex�o deve ser informada.");
			System.exit(0);
		}
			
		if (user.isEmpty() || user.equals("")) {
			LOGGER.error("O Usu�rio de Conex�o deve ser informado.");
			System.exit(0);
		}
		
		if (password.isEmpty() || password.equals("")) {
			LOGGER.error("A Senha de Conex�o deve ser informada.");
			System.exit(0);
		}
		
		Connection connection = ConnectionDatabase.getConnection(args[0], args[1], args[2]);
		
		IntegracaoBO.directory = ServerDirectoryIntegBO.getServerDirectoryIntegBO(connection);
		
		if(!IntegracaoBO.directory.isEmpty() && !IntegracaoBO.directory.equals("")) {
		
			LOGGER.info("Verificando diret�rio: " + IntegracaoBO.directory);
			
			ListFiles listFiles = new ListFiles();
			Set<String> list = listFiles.listFilesUsingDirectoryStream(IntegracaoBO.directory);
			
			if(list.size() > 0) {
				ReadFile readFile = new ReadFile();
				
				Iterator<String> it = list.iterator();
				
				while (it.hasNext()) {
					String f = IntegracaoBO.directory + "/" + it.next();
					
					LOGGER.info("Analisando o Arquivo: " + f);
					
					ArrayList<FileItems> listItems = readFile.readFileCSV(f);
					
					if(listItems.size() > 0) {
						int i = 1;
						
						for(FileItems item : listItems) {
							if (TpeIntegBO.insert(connection, f, item, i)) {
								LOGGER.info("Registro inclu�do com sucesso. Arquivo: " + f + " - Linha: " + String.valueOf(i));
							} else {
								LOGGER.warn("Registro n�o inclu�do, verifique o log. Arquivo: " + f + " - Linha: " + String.valueOf(i));
							}
							i++;
						}
					}
					
					LOGGER.info("Finalizando o Arquivo: " + f);
					
					new File(f).renameTo(new File(f + "$"));
				}
			} else {
				LOGGER.info("N�o existem arquivos no diret�rio: " + IntegracaoBO.directory);
			}
		} else {
			LOGGER.error("Verifique o par�metro 'TPEDIRINTEGBO'.");
			System.exit(0);
		}
		connection.close();
		System.exit(0);
	}

}
